/**
 * Copyright (C) 2011 Philippe Rétornaz
 *
 * Based on twl4030-pwrbutton driver by:
 *     Peter De Schrijver <peter.de-schrijver@nokia.com>
 *     Felipe Balbi <felipe.balbi@nokia.com>
 *
 * This file is subject to the terms and conditions of the GNU General
 * Public License. See the file "COPYING" in the main directory of this
 * archive for more details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335  USA
 */


#define DEBUG

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/mfd/mc13783.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/regmap.h>
#include "../../mfd/mc13xxx.h"

struct mc34708_pwrb {
	struct input_dev *pwr;
	struct mc13xxx *mc34708;
#define MC13783_PWRB_B1_POL_INVERT	(1 << 0)
#define MC13783_PWRB_B2_POL_INVERT	(1 << 1)
	int flags;
	unsigned short keymap[2];
        int virqs[2];
};

#define MC13783_REG_INTERRUPT_SENSE_1		5
#define MC13783_IRQSENSE1_ONOFD1S		(1 << 3)
#define MC13783_IRQSENSE1_ONOFD2S		(1 << 4)


#define MC13783_REG_POWER_CONTROL_2		15
#define MC13783_POWER_CONTROL_2_ON1BDBNC	4
#define MC13783_POWER_CONTROL_2_ON2BDBNC	6
#define MC13783_POWER_CONTROL_2_ON1BRSTEN	(1 << 1)
#define MC13783_POWER_CONTROL_2_ON2BRSTEN	(1 << 2)


static irqreturn_t button_1_irq(int irq, void *_priv)
{
	struct mc34708_pwrb *priv = _priv;
	int val;

	mc13xxx_irq_ack(priv->mc34708, irq);
	mc13xxx_reg_read(priv->mc34708, MC13783_REG_INTERRUPT_SENSE_1, &val);

        val = val & MC13783_IRQSENSE1_ONOFD1S ? 1 : 0;
        if (priv->flags & MC13783_PWRB_B1_POL_INVERT)
                val ^= 1;

        input_report_key(priv->pwr, priv->keymap[0], val);

	input_sync(priv->pwr);

	return IRQ_HANDLED;
}

static irqreturn_t button_2_irq(int irq, void *_priv)
{
	struct mc34708_pwrb *priv = _priv;
	int val;

	mc13xxx_irq_ack(priv->mc34708, irq);
        mc13xxx_reg_read(priv->mc34708, MC13783_REG_INTERRUPT_SENSE_1, &val);
        val = val & MC13783_IRQSENSE1_ONOFD2S ? 1 : 0;
        if (priv->flags & MC13783_PWRB_B2_POL_INVERT)
                val ^= 1;
        input_report_key(priv->pwr, priv->keymap[1], val);
	input_sync(priv->pwr);

	return IRQ_HANDLED;
}



#ifdef CONFIG_OF
static struct mc13xxx_buttons_platform_data *mc34708_parse_dt(struct device *dev)
{
	struct mc13xxx_buttons_platform_data *pdata;

	pdata = devm_kzalloc(dev, sizeof(*pdata), GFP_KERNEL);
	if (!pdata) {
		dev_err(dev, "failed to allocate platform data\n");
		return ERR_PTR(-ENOMEM);
	}
	pdata->b1on_flags = MC13783_BUTTON_DBNC_150MS | MC13783_BUTTON_ENABLE;
        pdata->b2on_flags = MC13783_BUTTON_DBNC_150MS | MC13783_BUTTON_ENABLE;
        pdata->b1on_key = KEY_POWER;
        pdata->b2on_key = KEY_ESC;
            
	return pdata;
}
#endif

static int mc34708_pwrbutton_probe(struct platform_device *pdev)
{
	const struct mc13xxx_buttons_platform_data *pdata = mc34708_parse_dt(&pdev->dev);
	struct mc13xxx *mc34708 = dev_get_drvdata(pdev->dev.parent);
	struct input_dev *pwr;
	struct mc34708_pwrb *priv;
	int err = 0;
	int reg = 0;

	if (!pdata) {
		dev_err(&pdev->dev, "missing platform data\n");
		return -ENODEV;
	}

	pwr = input_allocate_device();
	if (!pwr) {
		dev_dbg(&pdev->dev, "Can't allocate power button\n");
		return -ENOMEM;
	}

	priv = kzalloc(sizeof(*priv), GFP_KERNEL);
	if (!priv) {
		err = -ENOMEM;
		dev_dbg(&pdev->dev, "Can't allocate power button\n");
		goto free_input_dev;
	}

	reg |= (pdata->b1on_flags & 0x3) << MC13783_POWER_CONTROL_2_ON1BDBNC;
	reg |= (pdata->b2on_flags & 0x3) << MC13783_POWER_CONTROL_2_ON2BDBNC;
	
	priv->pwr = pwr;
	priv->mc34708 = mc34708;

	mc13xxx_lock(mc34708);

	if (pdata->b1on_flags & MC13783_BUTTON_ENABLE) {
		priv->keymap[0] = pdata->b1on_key;
		if (pdata->b1on_key != KEY_RESERVED)
			__set_bit(pdata->b1on_key, pwr->keybit);

		if (pdata->b1on_flags & MC13783_BUTTON_POL_INVERT)
			priv->flags |= MC13783_PWRB_B1_POL_INVERT;

		if (pdata->b1on_flags & MC13783_BUTTON_RESET_EN)
			reg |= MC13783_POWER_CONTROL_2_ON1BRSTEN;

		err = mc13xxx_irq_request(mc34708, MC13783_IRQ_ONOFD1,
					  button_1_irq, "b1on", priv);
		if (err) {
			dev_dbg(&pdev->dev, "Can't request irq\n");
			goto free_priv;
		}
		priv->virqs[0] = regmap_irq_get_virq(mc34708->irq_data, MC13783_IRQ_ONOFD1);
	}

	if (pdata->b2on_flags & MC13783_BUTTON_ENABLE) {
		priv->keymap[1] = pdata->b2on_key;
		if (pdata->b2on_key != KEY_RESERVED)
			__set_bit(pdata->b2on_key, pwr->keybit);

		if (pdata->b2on_flags & MC13783_BUTTON_POL_INVERT)
			priv->flags |= MC13783_PWRB_B2_POL_INVERT;

		if (pdata->b2on_flags & MC13783_BUTTON_RESET_EN)
			reg |= MC13783_POWER_CONTROL_2_ON2BRSTEN;

		err = mc13xxx_irq_request(mc34708, MC13783_IRQ_ONOFD2,
					  button_2_irq, "b2on", priv);
		if (err) {
			dev_dbg(&pdev->dev, "Can't request irq\n");
			goto free_irq_b1;
		}
		priv->virqs[1] = regmap_irq_get_virq(mc34708->irq_data, MC13783_IRQ_ONOFD2);
	}
        
	device_init_wakeup(&pdev->dev, 1);
	mc13xxx_reg_rmw(mc34708, MC13783_REG_POWER_CONTROL_2, 0x3FE, reg);

        /* set GLBRSTTMR to 0 */
        mc13xxx_reg_rmw(mc34708, MC13783_REG_POWER_CONTROL_2, 0x301, 0x101);
        

	mc13xxx_unlock(mc34708);

	pwr->name = "mc34708_pwrbutton";
	pwr->phys = "mc34708_pwrbutton/input0";
	pwr->dev.parent = &pdev->dev;

	pwr->keycode = priv->keymap;
	pwr->keycodemax = ARRAY_SIZE(priv->keymap);
	pwr->keycodesize = sizeof(priv->keymap[0]);
	__set_bit(EV_KEY, pwr->evbit);

	err = input_register_device(pwr);
	if (err) {
		dev_dbg(&pdev->dev, "Can't register power button: %d\n", err);
		goto free_irq;
	}

	platform_set_drvdata(pdev, priv);

	return 0;

free_irq:
	mc13xxx_lock(mc34708);
	if (pdata->b2on_flags & MC13783_BUTTON_ENABLE)
		mc13xxx_irq_free(mc34708, MC13783_IRQ_ONOFD2, priv);

free_irq_b1:
	if (pdata->b1on_flags & MC13783_BUTTON_ENABLE)
		mc13xxx_irq_free(mc34708, MC13783_IRQ_ONOFD1, priv);

free_priv:
	mc13xxx_unlock(mc34708);
	kfree(priv);

free_input_dev:
	input_free_device(pwr);

	return err;
}

static int mc34708_pwrbutton_remove(struct platform_device *pdev)
{
	struct mc34708_pwrb *priv = platform_get_drvdata(pdev);
	const struct mc13xxx_buttons_platform_data *pdata;

	pdata = dev_get_platdata(&pdev->dev);

	mc13xxx_lock(priv->mc34708);

	if (pdata->b2on_flags & MC13783_BUTTON_ENABLE)
		mc13xxx_irq_free(priv->mc34708, MC13783_IRQ_ONOFD2, priv);
	if (pdata->b1on_flags & MC13783_BUTTON_ENABLE)
		mc13xxx_irq_free(priv->mc34708, MC13783_IRQ_ONOFD1, priv);

	mc13xxx_unlock(priv->mc34708);

	input_unregister_device(priv->pwr);
	kfree(priv);

	return 0;
}


#ifdef CONFIG_PM_SLEEP
static int mc34708_suspend(struct device *dev)
{
        struct platform_device *pdev = to_platform_device(dev);
        struct mc34708_pwrb *priv = platform_get_drvdata(pdev);
        
	if (device_may_wakeup(dev)) {
            dev_dbg(dev, "device_may_wakeup 1\n");
                enable_irq_wake(priv->virqs[0]);
                enable_irq_wake(priv->virqs[1]);
	}else{
            dev_dbg(dev, "device_may_wakeup 0\n");
        }

	return 0;
}

static int mc34708_resume(struct device *dev)
{       
        struct platform_device *pdev = to_platform_device(dev);
        struct mc34708_pwrb *priv = platform_get_drvdata(pdev);
        
	if (device_may_wakeup(dev)) {
                dev_dbg(dev, "device_may_wakeup 1\n");
                disable_irq_wake(priv->virqs[0]);
                disable_irq_wake(priv->virqs[1]);
	}else{
            dev_dbg(dev, "device_may_wakeup 0\n");
        }

	return 0;
}

static SIMPLE_DEV_PM_OPS(mc34708_pm_ops,mc34708_suspend, mc34708_resume);

#endif



static struct platform_driver mc34708_pwrbutton_driver = {
	.probe		= mc34708_pwrbutton_probe,
	.remove		= mc34708_pwrbutton_remove,
	.driver		= {
		.name	= "mc34708-pwrbutton",
#ifdef CONFIG_PM_SLEEP
                .pm = &mc34708_pm_ops,
#endif
	},
};

module_platform_driver(mc34708_pwrbutton_driver);

MODULE_ALIAS("platform:mc34708-pwrbutton");
MODULE_DESCRIPTION("MC13783 Power Button");
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Philippe Retornaz");
